<?php
class Preiser_Default_Init
{

    public function init()
    {
        add_action( 'wp_enqueue_scripts', array($this, 'preiser_theme_enqueue_scripts_styles') );
        add_action( 'init', array($this, 'register_custom_navigation_menus') );
    }
    public function preiser_theme_enqueue_scripts_styles() {
        wp_enqueue_style( 'divi-parent-style', get_template_directory_uri() . '/style.css' );
        wp_enqueue_style( 'global-preiser-styles', get_stylesheet_directory_uri() . '/css/global.css', 'divi-parent-style',
            false );
        wp_enqueue_script( 'global-preiser-scripts', get_stylesheet_directory_uri() . '/js/theme.min.js', array('jquery'), false, true );
    }

    public function register_custom_navigation_menus()
    {
        register_nav_menu('account-menu',__( 'Account Menu' ));
        register_nav_menu('top-bar-menu',__( 'Top Bar Menu' ));
    }


}

$default = new Preiser_Default_Init();
$default->init();

