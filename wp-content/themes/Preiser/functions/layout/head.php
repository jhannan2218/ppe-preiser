<?php

class Preiser_Head
{

    public function init()
    {
        add_action('before_et_head_meta', array($this, 'add_meta_charset_to_head'), 10);
        add_action('before_et_head_meta', array($this, 'add_elegant_themes_functions_to_head'), 20);

        add_action('after_et_head_meta', array($this, 'get_pingback_meta_tag'), 10);
        add_action('after_et_head_meta', array($this, 'get_js_init_script'), 10);
    }

    public function add_meta_charset_to_head()
    {
        $charset = get_bloginfo('charset');
        echo '<meta charset="' . $charset . '" />';
    }

    public function add_elegant_themes_functions_to_head()
    {
        elegant_description();
        elegant_keywords();
        elegant_canonical();
    }

    public function get_pingback_meta_tag()
    {
        $pingback = get_bloginfo('pingback_url');
        echo '<link rel="pingback" href="' . $pingback . '" />';
    }

    public function get_js_init_script()
    {

        echo "<script type='text/javascript'>document.documentElement.className = 'js';</script>";
    }

}

$header = new Preiser_Head();
$header->init();