<?php

class Preiser_Header
{

    public function init()
    {

        add_action('init', array($this, 'remove_et_add_mobile_navigation'));

        add_action( 'et_header_top', array($this, 'custom_et_add_mobile_navigation'));

        add_action('header_main_area', array($this, 'get_logo'));
        //add_action('header_main_area', array($this, 'get_cart_icon'));

        add_action('header_nav_area', array($this, 'get_header_navigation'));

        //add_action('header_search_icon', array($this, 'get_search_icon'));

        //add_action('header_search_area', array($this, 'get_header_search'));

        add_action('header_top_bar', array($this, 'get_header_top_bar'));
    }

    public function get_et_slide_header()
    {
        $et_slide_header = 'slide' === et_get_option( 'header_style', 'left' ) || 'fullscreen' === et_get_option('header_style', 'left' ) ? true : false;
        return $et_slide_header;
    }

    public function get_logo()
    {
        $logo = ( $user_logo = et_get_option( 'divi_logo' ) ) && ! empty( $user_logo )
            ? $user_logo
            : $template_directory_uri . '/images/logo.png';

        ob_start();
        ?>
        <div class="logo_container">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img src="<?php echo esc_attr( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" id="logo" data-height-percentage="<?php echo esc_attr( et_get_option( 'logo_height', '54' ) ); ?>" />
            </a>
        </div>
        <?php
        $logo_container = ob_get_clean();

        /**
         * Filters the HTML output for the logo container.
         *
         * @since 3.10
         *
         * @param string $logo_container
         */
        echo et_core_intentionally_unescaped( apply_filters( 'et_html_logo_container', $logo_container ), 'html' );
    }

    public function get_cart_icon()
    {
        get_template_part('partials/header/main/cart-link');
    }

    public function get_header_navigation()
    {
        $et_slide_header = $this->get_et_slide_header();
        if (!$et_slide_header || is_customize_preview()) :
          get_template_part('partials/header/main/nav');
        endif;
    }

    public function get_search_icon()
    {
      $et_slide_header = $this->get_et_slide_header();
      if ( ( false !== et_get_option( 'show_search_icon', true ) && ! $et_slide_header ) || is_customize_preview() ) :
        echo '<div id="et_top_search"><span id="et_search_icon"></span></div>';
      endif;
    }

    public function get_header_search()
    {
        get_template_part('partials/header/main/search');
    }

    public function get_header_top_bar()
    {
        get_template_part('partials/header/main/top_bar');
    }

    public function remove_et_add_mobile_navigation()
    {
        remove_action( 'et_header_top', 'et_add_mobile_navigation');
    }

    public function custom_et_add_mobile_navigation(){
        if ( is_customize_preview() || ( 'slide' !== et_get_option( 'header_style', 'left' ) && 'fullscreen' !== et_get_option( 'header_style', 'left' ) ) ) {
          ?>
                <div id="et_mobile_nav_menu">
                  <div class="mobile_nav closed">
                    <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
                  </div>
                </div>
          <?php
        }
    }

}

$header = new Preiser_Header();
$header->init();