<?php

class Preiser_JotForm
{

    function init()
    {

        if(!is_admin()) {
            add_shortcode('jotform', array($this, 'getCustomJotFormCode'));
        }

    }

    function getCustomJotFormCode($atts)
    {

        ob_start();
        $atts = shortcode_atts( array(
            'type' => 'contact',
            'application_label' => 'Organization Name'
        ), $atts, 'jotform');
        if(!empty($atts)) {
            $organization = $atts['application_label'];
            if($atts['type'] === 'contact') {
                include( locate_template('partials/module/contact-default.php', false, false) );
            }
            if($atts['type'] === 'classroom') {
                include( locate_template('partials/module/classroom-form.php', false, false) );
            }
            if($atts['type'] === 'quote') {
                include( locate_template('partials/module/request-a-quote.php', false, false) );
            }
        }

        $output = ob_get_clean();
        return $output;

    }
}

$jotForm = new Preiser_JotForm();
$jotForm->init();