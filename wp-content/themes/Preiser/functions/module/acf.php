<?php

class PreiserACF {

    public function init()
    {
        add_action('admin_menu', array($this, 'add_custom_options_page'), 99);
    }

    public function add_custom_options_page()
    {
        if( function_exists('acf_add_options_page') ) {

            acf_add_options_page();

        }
    }

}

$acfOptions = new PreiserACF();
$acfOptions->init();