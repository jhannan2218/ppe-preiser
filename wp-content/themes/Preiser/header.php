<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php
  do_action('before_et_head_meta');
	do_action( 'et_head_meta' );
	$template_directory_uri = get_template_directory_uri();
	do_action('after_et_head_meta');
	wp_head();
?>
</head>
<body <?php body_class(); ?>>
<?php
	$product_tour_enabled = et_builder_is_product_tour_enabled();
	$page_container_style = $product_tour_enabled ? ' style="padding-top: 0px;"' : '';
?>
  <div id="page-container"<?php echo et_core_intentionally_unescaped( $page_container_style, 'fixed_string' ); ?>>
<?php
	if ( $product_tour_enabled || is_page_template( 'page-template-blank.php' ) ) {
		return;
	}
	$et_secondary_nav_items = et_divi_get_top_nav_items();
	$et_phone_number = $et_secondary_nav_items->phone_number;
	$et_email = $et_secondary_nav_items->email;
	$et_contact_info_defined = $et_secondary_nav_items->contact_info_defined;
	$show_header_social_icons = $et_secondary_nav_items->show_header_social_icons;
	$et_secondary_nav = $et_secondary_nav_items->secondary_nav;
	$et_top_info_defined = $et_secondary_nav_items->top_info_defined;
	$et_slide_header = 'slide' === et_get_option( 'header_style', 'left' ) || 'fullscreen' === et_get_option( 'header_style', 'left' ) ? true : false;

	if ( $et_top_info_defined && ! $et_slide_header || is_customize_preview() ) :
    get_template_part('partials/header/one');
	endif; // true ==== $et_top_info_defined

	if ( $et_slide_header || is_customize_preview() ) :
    get_template_part('partials/header/two');
	endif; // true ==== $et_slide_header
  get_template_part('partials/header/main');
?>
		<main id="et-main-area">
<?php
  /**
   * Fires after the header, before the main content is output.
   *
   * @since 3.10
   */
  do_action( 'et_before_main_content' );
