</main><!--#et-main-area-->
<?php
if ( et_theme_builder_overrides_layout( ET_THEME_BUILDER_HEADER_LAYOUT_POST_TYPE ) || et_theme_builder_overrides_layout( ET_THEME_BUILDER_FOOTER_LAYOUT_POST_TYPE ) ) {
    // Skip rendering anything as this partial is being buffered anyway.
    // In addition, avoids get_sidebar() issues since that uses
    // locate_template() with require_once.
    return;
}
do_action( 'et_after_main_content' );

if ( 'on' === et_get_option( 'divi_back_to_top', 'false' ) ) :
?>
	<span class="et_pb_scroll_top et-pb-icon"></span>
<?php
endif;
if ( ! is_page_template( 'page-template-blank.php' ) ) :
?>
<footer id="main-footer">
  <?php get_sidebar( 'footer' ); ?>
<?php
endif; // ! is_page_template( 'page-template-blank.php' )
?>
</footer>
<?php
  get_template_part('partials/module/modal');
?>
</div> <!-- #page-container -->
<?php wp_footer(); ?>
<?php get_template_part('partials/footer/linkedin'); ?>
</body>
</html>
