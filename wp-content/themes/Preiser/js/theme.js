jQuery(document).ready(function($) {

    var theme = {
        defaults:
        {
            domain: '',
            header:
            {
                container: $('#main-header')
            },
            modal:
            {
                isActive: true,
                isOpen: false,
                container: $('.popup-modal-container'),
                content: $('.popup-modal-container .popup-modal-content'),
                targetLabel: $('.popup-modal-container .popup-modal-content > p span'),
                button: $('.popup-modal-container .popup-modal-content .et_pb_button.continue'),
                cancelTrigger: $('.popup-modal-container .popup-modal-content .et_pb_button.cancel')
            },
            scrollTrigger: {
                trigger: $('.scroll-trigger a'),

            }
        },
        init: function()
        {
            var url = $('.logo_container > a').attr('href');
            theme.defaults.domain = theme.getDomain(url);
            /*
                Turning off until client approves
             */
            theme.externalLinkListener();
            theme.closeListeners();
        },
        getDomain: function(url)
        {
            if(url.length) {
                var domain = url.replace('http://','').replace('https://','').split(/[/?#]/)[0];
                if(domain.includes('tel:') || domain.includes('mailto:')) {
                    return theme.defaults.domain;
                } else {
                    return domain;
                }
            } else {
                return;
            }
        },
        externalLinkListener: function()
        {
            $('a').not('.popup-modal-content a.et_pb_button').on('click', theme.defaults.modal.container, function(){
                if(theme.defaults.modal.isActive) {
                    var url = $(this).attr('href');
                    var domain = theme.getDomain(url);
                    if(domain != theme.defaults.domain) {
                        console.log('external link!');
                        theme.modalSetup(url, domain);
                        theme.modalOpen();
                        return false;
                    }
                }
            });
            theme.defaults.modal.button.on('click', function() {
                theme.modalClose();
                theme.defaults.modal.isActive = false;
            });
        },
        closeListeners: function()
        {
            theme.defaults.modal.cancelTrigger.on('click', function(){
                theme.modalClose();
                return false;
            });
        },
        modalSetup: function(url, domain)
        {
            theme.defaults.modal.targetLabel.text(domain);
            theme.defaults.modal.button.append(' '+domain);
            theme.defaults.modal.button.attr('href', url);
        },
        modalOpen: function()
        {
            if(!theme.defaults.modal.isOpen) {
                theme.defaults.modal.isOpen = true;
            }
            $(theme.defaults.modal.container).addClass('open');
        },
        modalClose: function()
        {
            if(theme.defaults.modal.isOpen) {
                theme.defaults.modal.isOpen = false;
            }
            theme.defaults.modal.container.removeClass('open');
        },
        scrollTrigger: function()
        {
            var trigger = theme.defaults.scrollTrigger.trigger;
            trigger.on('click', function(){
                var target = $(this).attr('href');
                theme.scrollTo($('#'+target));
                return false;
            });
        },
        scrollTo: function(target)
        {
            $(document).animate({
                scrollTop: target.offset().top
            });
        }
    }
    theme.init();

});