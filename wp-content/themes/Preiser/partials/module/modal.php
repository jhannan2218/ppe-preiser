<div class="popup-modal-container">
    <div class="popup-modal-content">
      <p>You are now leaving this website to visit <span></span>. The product detail page will open in a new tab.</p>
      <a href="#" target="_blank" class="et_pb_button cancel">Cancel</a>
      <a href="#" target="_blank" class="et_pb_button continue">Continue to</a>
    </div>
</div>