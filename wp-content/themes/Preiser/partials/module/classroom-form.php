<script src="https://cdnjs.cloudflare.com/ajax/libs/punycode/1.4.1/punycode.min.js"></script>
<script src="https://cdn.jotfor.ms/static/prototype.forms.js" type="text/javascript"></script>
<script src="https://cdn.jotfor.ms/static/jotform.forms.js?3.3.19536" type="text/javascript"></script>
<script type="text/javascript">
    JotForm.init(function(){
        if (window.JotForm && JotForm.accessible) $('input_3').setAttribute('tabindex',0);
        if (window.JotForm && JotForm.accessible) $('input_4').setAttribute('tabindex',0);
        if (window.JotForm && JotForm.accessible) $('input_6').setAttribute('tabindex',0);
        if (window.JotForm && JotForm.accessible) $('input_8').setAttribute('tabindex',0);
        if (window.JotForm && JotForm.accessible) $('input_12').setAttribute('tabindex',0);
        JotForm.newDefaultTheme = false;
        JotForm.newPaymentUIForNewCreatedForms = true;
        JotForm.newPaymentUI = true;
        JotForm.alterTexts(undefined);
        JotForm.clearFieldOnHide="disable";
        JotForm.submitError="jumpToFirstError";
        /*INIT-END*/
    });

    JotForm.prepareCalculationsOnTheFly([null,null,{"name":"submit2","qid":"2","text":"Submit","type":"control_button"},{"description":"","name":"organizationName","qid":"3","subLabel":"","text":"Organization Name","type":"control_textbox"},{"description":"","name":"location","qid":"4","subLabel":"City\u002FState","text":"Location","type":"control_textbox"},null,{"description":"","name":"contactName","qid":"6","subLabel":"","text":"Contact Name","type":"control_textbox"},{"description":"","name":"contactEmail","qid":"7","subLabel":"","text":"Contact Email","type":"control_email"},{"description":"","name":"numberOf","qid":"8","subLabel":"","text":"Number of Students","type":"control_textbox"},null,{"description":"","name":"phoneNumber10","qid":"10","text":"Phone Number","type":"control_phone"},{"description":"","name":"ppeRequired","qid":"11","text":"PPE Required","type":"control_checkbox"},{"description":"","name":"commentsquestions","qid":"12","subLabel":"","text":"Comments\u002FQuestions","type":"control_textarea"},{"description":"","name":"pleaseVerify","qid":"13","text":"Please verify that you are human","type":"control_captcha"}]);
    setTimeout(function() {
        JotForm.paymentExtrasOnTheFly([null,null,{"name":"submit2","qid":"2","text":"Submit","type":"control_button"},{"description":"","name":"organizationName","qid":"3","subLabel":"","text":"Organization Name","type":"control_textbox"},{"description":"","name":"location","qid":"4","subLabel":"City\u002FState","text":"Location","type":"control_textbox"},null,{"description":"","name":"contactName","qid":"6","subLabel":"","text":"Contact Name","type":"control_textbox"},{"description":"","name":"contactEmail","qid":"7","subLabel":"","text":"Contact Email","type":"control_email"},{"description":"","name":"numberOf","qid":"8","subLabel":"","text":"Number of Students","type":"control_textbox"},null,{"description":"","name":"phoneNumber10","qid":"10","text":"Phone Number","type":"control_phone"},{"description":"","name":"ppeRequired","qid":"11","text":"PPE Required","type":"control_checkbox"},{"description":"","name":"commentsquestions","qid":"12","subLabel":"","text":"Comments\u002FQuestions","type":"control_textarea"},{"description":"","name":"pleaseVerify","qid":"13","text":"Please verify that you are human","type":"control_captcha"}]);}, 20);
</script>
<form class="jotform-form" action="https://submit.jotform.com/submit/202235023503034/" method="post" name="form_202235023503034" id="202235023503034" accept-charset="utf-8" autocomplete="on">
    <input type="hidden" name="formID" value="202235023503034" />
    <input type="hidden" id="JWTContainer" value="" />
    <input type="hidden" id="cardinalOrderNumber" value="" />
    <div role="main" class="form-all">
        <ul class="form-section page-section">
          <div class="column">
            <li class="form-line" data-type="control_textbox" id="id_3">
                <label class="form-label form-label-top form-label-auto" id="label_3" for="input_3"><?php echo $organization; ?></label>
                <div id="cid_3" class="form-input-wide">
                    <input type="text" id="input_3" name="q3_organizationName" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" aria-labelledby="label_3" />
                </div>
            </li>
            <li class="form-line" data-type="control_textbox" id="id_4">
                <label class="form-label form-label-top form-label-auto" id="label_4" for="input_4"> Location </label>
                <div id="cid_4" class="form-input-wide">
                  <span class="form-sub-label-container " style="vertical-align:top">
                    <input type="text" id="input_4" name="q4_location" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" aria-labelledby="label_4 sublabel_input_4" />
                    <label class="form-sub-label" for="input_4" id="sublabel_input_4" style="min-height:13px" aria-hidden="false"> City/State </label>
                  </span>
                </div>
            </li>
            <li class="form-line" data-type="control_textbox" id="id_6">
                <label class="form-label form-label-top form-label-auto" id="label_6" for="input_6"> Contact Name </label>
                <div id="cid_6" class="form-input-wide">
                    <input type="text" id="input_6" name="q6_contactName" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" aria-labelledby="label_6" />
                </div>
            </li>
            <li class="form-line" data-type="control_email" id="id_7">
                <label class="form-label form-label-top form-label-auto" id="label_7" for="input_7"> Contact Email </label>
                <div id="cid_7" class="form-input-wide">
                    <input type="email" id="input_7" name="q7_contactEmail" class="form-textbox validate[Email]" size="30" value="" data-component="email" aria-labelledby="label_7" />
                </div>
            </li>
            <li class="form-line" data-type="control_textbox" id="id_8">
                <label class="form-label form-label-top form-label-auto" id="label_8" for="input_8"> Number of Students </label>
                <div id="cid_8" class="form-input-wide">
                    <input type="text" id="input_8" name="q8_numberOf" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" aria-labelledby="label_8" />
                </div>
            </li>
            <li class="form-line" data-type="control_phone" id="id_10">
                <label class="form-label form-label-top form-label-auto" id="label_10" for="input_10_area"> Phone Number </label>
                <div id="cid_10" class="form-input-wide">
                    <div data-wrapper-react="true">
                      <span class="form-sub-label-container " data-input-type="phone">
                        <input type="tel" id="input_10_phone" name="q10_phoneNumber10" class="form-textbox" size="12" value="" data-component="phone" aria-labelledby="label_10 sublabel_10_phone" />
                      </span>
                    </div>
                </div>
            </li>
          </div>
          <div class="column">
            <li class="form-line" data-type="control_checkbox" id="id_11">
                <label class="form-label form-label-top form-label-auto" id="label_11" for="input_11"> PPE Required </label>
                <div id="cid_11" class="form-input-wide">
                    <div class="form-single-column" role="group" aria-labelledby="label_11" data-component="checkbox">
                      <span class="form-checkbox-item">
                        <span class="dragger-item">

                        </span>
                        <input type="checkbox" class="form-checkbox" id="input_11_0" name="q11_ppeRequired[]" value="Gloves" />
                        <label id="label_input_11_0" for="input_11_0"> Gloves </label>
                      </span>
                      <span class="form-checkbox-item" style="clear:left">
                        <span class="dragger-item">

                        </span>
                        <input type="checkbox" class="form-checkbox" id="input_11_1" name="q11_ppeRequired[]" value="Disposable Masks" />
                        <label id="label_input_11_1" for="input_11_1"> Disposable Masks </label>
                      </span>
                      <span class="form-checkbox-item" style="clear:left">
                        <span class="dragger-item">

                        </span>
                        <input type="checkbox" class="form-checkbox" id="input_11_2" name="q11_ppeRequired[]" value="Reusable/Washable Masks" />
                        <label id="label_input_11_2" for="input_11_2"> Reusable/Washable Masks </label>
                      </span>
                      <span class="form-checkbox-item" style="clear:left">
                        <span class="dragger-item">

                        </span>
                        <input type="checkbox" class="form-checkbox" id="input_11_3" name="q11_ppeRequired[]" value="Face Shields" />
                        <label id="label_input_11_3" for="input_11_3"> Face Shields </label>
                      </span>
                      <span class="form-checkbox-item" style="clear:left">
                        <span class="dragger-item">

                        </span>
                        <input type="checkbox" class="form-checkbox" id="input_11_4" name="q11_ppeRequired[]" value="Sanitizer" />
                        <label id="label_input_11_4" for="input_11_4"> Sanitizer </label>
                      </span>
                      <span class="form-checkbox-item" style="clear:left">
                        <span class="dragger-item">

                        </span>
                        <input type="checkbox" class="form-checkbox" id="input_11_5" name="q11_ppeRequired[]" value="Wipes" />
                        <label id="label_input_11_5" for="input_11_5"> Wipes </label>
                      </span>
                      <span class="form-checkbox-item" style="clear:left">
                        <span class="dragger-item">

                        </span>
                        <input type="checkbox" class="form-checkbox" id="input_11_6" name="q11_ppeRequired[]" value="Separation Barriers" />
                        <label id="label_input_11_6" for="input_11_6"> Separation Barriers </label>
                      </span>
                      <span class="form-checkbox-item" style="clear:left">
                        <span class="dragger-item">

                        </span>
                        <input type="checkbox" class="form-checkbox" id="input_11_7" name="q11_ppeRequired[]" value="Thermometers" />
                        <label id="label_input_11_7" for="input_11_7"> Thermometers </label>
                      </span>
                      <span class="form-checkbox-item" style="clear:left">
                        <input type="checkbox" class="form-checkbox-other form-checkbox" name="q11_ppeRequired[other]" id="other_11" value="other" aria-label="Other" />
                        <label id="label_other_11" style="text-indent:0" for="other_11">  </label>
                        <input type="text" class="form-checkbox-other-input form-textbox" name="q11_ppeRequired[other]" data-otherhint="Other" size="15" id="input_11" placeholder="Other" />
                      </span>
                    </div>
                </div>
            </li>
            <li class="form-line" data-type="control_textarea" id="id_12">
                <label class="form-label form-label-top form-label-auto" id="label_12" for="input_12"> Comments/Questions </label>
                <div id="cid_12" class="form-input-wide">
                    <textarea id="input_12" class="form-textarea" name="q12_commentsquestions" cols="40" rows="6" data-component="textarea" aria-labelledby="label_12"></textarea>
                </div>
            </li>
            <li class="form-line jf-required captcha" data-type="control_captcha" id="id_13">
                <label class="form-label form-label-top form-label-auto" id="label_13" for="input_13">
                    Please verify that you are human
                    <span class="form-required">*</span>
                </label>
                <div id="cid_13" class="form-input-wide jf-required">
                    <section data-wrapper-react="true">
                        <div id="recaptcha_input_13" data-component="recaptcha" data-callback="recaptchaCallbackinput_13" data-expired-callback="recaptchaExpiredCallbackinput_13">
                        </div>
                        <input type="hidden" id="input_13" class="hidden validate[required]" name="recaptcha_visible" required="" />
                        <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?render=explicit&amp;onload=recaptchaLoadedinput_13"></script>
                        <script type="text/javascript">
                            var recaptchaLoadedinput_13 = function()
                            {
                                window.grecaptcha.render($("recaptcha_input_13"), {
                                    sitekey: '6LdU3CgUAAAAAB0nnFM3M3T0sy707slYYU51RroJ',
                                });
                                var grecaptchaBadge = document.querySelector('.grecaptcha-badge');
                                if (grecaptchaBadge)
                                {
                                    grecaptchaBadge.style.boxShadow = 'gray 0px 0px 2px';
                                }
                            };

                            /**
                             * Called when the reCaptcha verifies the user is human
                             * For invisible reCaptcha;
                             *   Submit event is stopped after validations and recaptcha is executed.
                             *   If a challenge is not displayed, this will be called right after grecaptcha.execute()
                             *   If a challenge is displayed, this will be called when the challenge is solved successfully
                             *   Submit is triggered to actually submit the form since it is stopped before.
                             */
                            var recaptchaCallbackinput_13 = function()
                            {
                                var isInvisibleReCaptcha = false;
                                var hiddenInput = $("input_13");
                                hiddenInput.setValue(1);
                                if (!isInvisibleReCaptcha)
                                {
                                    if (hiddenInput.validateInput)
                                    {
                                        hiddenInput.validateInput();
                                    }
                                }
                                else
                                {
                                    triggerSubmit(hiddenInput.form)
                                }

                                function triggerSubmit(formElement)
                                {
                                    var button = formElement.ownerDocument.createElement('input');
                                    button.style.display = 'none';
                                    button.type = 'submit';
                                    formElement.appendChild(button).click();
                                    formElement.removeChild(button);
                                }
                            }

                            // not really required for invisible recaptcha
                            var recaptchaExpiredCallbackinput_13 = function()
                            {
                                var hiddenInput = $("input_13");
                                hiddenInput.writeAttribute("value", false);
                                if (hiddenInput.validateInput)
                                {
                                    hiddenInput.validateInput();
                                }
                            }
                        </script>
                    </section>
                </div>
            </li>
            <li class="form-line" data-type="control_button" id="id_2">
                <div id="cid_2" class="form-input-wide">
                    <div style="margin-left:156px" data-align="auto" class="form-buttons-wrapper form-buttons-auto   jsTest-button-wrapperField">
                        <button id="input_2" type="submit" class="form-submit-button submit-button jf-form-buttons
                        jsTest-submitField et_pb_button" data-component="button" data-content="">
                            Submit
                        </button>
                    </div>
                </div>
            </li>
            <li style="display:none">
                Should be Empty:
                <input type="text" name="website" value="" />
            </li>
          </div>
        </ul>
    </div>
    <input type="hidden" id="simple_spc" name="simple_spc" value="202235023503034" />
    <script type="text/javascript">
        document.getElementById("si" + "mple" + "_spc").value = "202235023503034-202235023503034";
    </script>
</form>