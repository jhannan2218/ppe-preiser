<?php
$et_secondary_nav_items = et_divi_get_top_nav_items();
$et_phone_number = $et_secondary_nav_items->phone_number;
$et_email = $et_secondary_nav_items->email;
$et_contact_info_defined = $et_secondary_nav_items->contact_info_defined;
$show_header_social_icons = $et_secondary_nav_items->show_header_social_icons;
$et_secondary_nav = $et_secondary_nav_items->secondary_nav;
$et_top_info_defined = $et_secondary_nav_items->top_info_defined;
$et_slide_header = 'slide' === et_get_option( 'header_style', 'left' ) || 'fullscreen' === et_get_option(
    'header_style', 'left' ) ? true : false;

ob_start();
?>
<header id="top-header"<?php echo $et_top_info_defined ? '' : 'style="display: none;"'; ?>>
    <div class="container">

        <?php if ( $et_contact_info_defined ) : ?>

            <div id="et-info">
                <?php if ( '' !== ( $et_phone_number = et_get_option( 'phone_number' ) ) ) : ?>
                    <span id="et-info-phone">Have Questions?  Call Us:  <a href="tel:<?php echo et_core_esc_previously(
                            et_sanitize_html_input_text( $et_phone_number ) ); ?>"><?php echo
                            et_core_esc_previously(
                            et_sanitize_html_input_text( $et_phone_number ) ); ?></a></span>
                <?php endif; ?>

                <?php /*
                <?php if ( '' !== ( $et_email = et_get_option( 'header_email' ) ) ) : ?>
                    <a href="<?php echo esc_attr( 'mailto:' . $et_email ); ?>"><span id="et-info-email"><?php echo esc_html( $et_email ); ?></span></a>
                <?php endif; ?>
                    */ ?>

                <?php
                if ( true === $show_header_social_icons ) {
                    get_template_part( 'includes/social_icons', 'header' );
                } ?>
            </div> <!-- #et-info -->

        <?php endif; // true === $et_contact_info_defined ?>

        <div id="et-secondary-menu">
            <?php
            if ( ! $et_contact_info_defined && true === $show_header_social_icons ) {
                get_template_part( 'includes/social_icons', 'header' );
            } else if ( $et_contact_info_defined && true === $show_header_social_icons ) {
                ob_start();

                get_template_part( 'includes/social_icons', 'header' );

                $duplicate_social_icons = ob_get_contents();

                ob_end_clean();

                printf(
                    '<div class="et_duplicate_social_icons">
								%1$s
							</div>',
                    et_core_esc_previously( $duplicate_social_icons )
                );
            }

            if ( '' !== $et_secondary_nav ) {
                echo et_core_esc_wp( $et_secondary_nav );
            }

            et_show_cart_total();
            ?>
        </div> <!-- #et-secondary-menu -->

    </div> <!-- .container -->
</header> <!-- #top-header -->
<?php
$top_header = ob_get_clean();

/**
 * Filters the HTML output for the top header.
 *
 * @since 3.10
 *
 * @param string $top_header
 */
echo et_core_intentionally_unescaped( apply_filters( 'et_html_top_header', $top_header ), 'html' );
?>