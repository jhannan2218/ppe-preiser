<?php
if(has_nav_menu('top-bar-menu')) :
?>
<div class="top-bar top-bar-header">
    <div class="container">
        <?php
          $args = array(
              'theme_location' => 'top-bar-menu',
              'container' => false
          );
          wp_nav_menu($args);
        ?>
    </div>
</div>
<?php
endif;