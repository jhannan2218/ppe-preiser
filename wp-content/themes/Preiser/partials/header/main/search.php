<div class="et_search_outer">
    <div class="container et_search_form_container">
        <form role="search" method="get" class="et-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <?php
            printf( '<input type="search" class="et-search-field" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
                esc_attr__( 'Search &hellip;', 'Divi' ),
                get_search_query(),
                esc_attr__( 'Search for:', 'Divi' )
            );

            /**
             * Fires inside the search form element, just before its closing tag.
             *
             * @since ??
             */
            do_action( 'et_search_form_fields' );
            ?>
        </form>
        <span class="et_close_search_field"></span>
    </div><!--/et_search_form_container-->
</div><!--/et_search_outer-->