<?php
$et_secondary_nav_items = et_divi_get_top_nav_items();
$et_phone_number = $et_secondary_nav_items->phone_number;
$et_email = $et_secondary_nav_items->email;
$et_contact_info_defined = $et_secondary_nav_items->contact_info_defined;
$show_header_social_icons = $et_secondary_nav_items->show_header_social_icons;
$et_secondary_nav = $et_secondary_nav_items->secondary_nav;
$et_top_info_defined = $et_secondary_nav_items->top_info_defined;
$et_slide_header = 'slide' === et_get_option( 'header_style', 'left' ) || 'fullscreen' === et_get_option('header_style', 'left' ) ? true : false;

ob_start();
?>
<header id="main-header" data-height-onload="<?php echo esc_attr( et_get_option( 'menu_height', '66' ) ); ?>">
  <?php do_action('header_top_bar'); ?>
  <div class="header-main">
    <div class="container">
        <?php do_action('header_main_area'); ?>
    </div> <!-- .container -->
  </div>
  <?php if( has_nav_menu('primary-menu') || has_nav_menu('account-menu') ) : ?>
  <div id="et-top-navigation" data-height="<?php echo esc_attr( et_get_option( 'menu_height', '66' ) ); ?>" data-fixed-height="<?php echo esc_attr( et_get_option( 'minimized_menu_height', '40' ) ); ?>">
      <?php do_action('header_nav_area'); ?>

      <?php
      /*
      if ( ! $et_top_info_defined && ( ! $et_slide_header || is_customize_preview() ) ) {
          et_show_cart_total( array(
              'no_text' => true,
          ) );
      }
      */
      ?>

      <?php if ( $et_slide_header || is_customize_preview() ) : ?>
        <span class="mobile_menu_bar et_pb_header_toggle et_toggle_<?php echo esc_attr( et_get_option( 'header_style', 'left' ) ); ?>_menu"></span>
      <?php endif; ?>

      <?php do_action('header_search_icon'); ?>

      <?php
      /**
       * Fires at the end of the 'et-top-navigation' element, just before its closing tag.
       *
       * @since 1.0
       */
      do_action( 'et_header_top' );
      ?>
  </div> <!-- #et-top-navigation -->
  <?php endif; ?>
  <?php do_action('header_search_area'); ?>
</header> <!-- #main-header -->
<?php
  $main_header = ob_get_clean();
  /**
  * Filters the HTML output for the main header.
  *
  * @since 3.10
  *
  * @param string $main_header
  */
  echo et_core_intentionally_unescaped( apply_filters( 'et_html_main_header', $main_header ), 'html' );
?>