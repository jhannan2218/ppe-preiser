<?php
$et_secondary_nav_items = et_divi_get_top_nav_items();
$et_phone_number = $et_secondary_nav_items->phone_number;
$et_email = $et_secondary_nav_items->email;
$et_contact_info_defined = $et_secondary_nav_items->contact_info_defined;
$show_header_social_icons = $et_secondary_nav_items->show_header_social_icons;
$et_secondary_nav = $et_secondary_nav_items->secondary_nav;
$et_top_info_defined = $et_secondary_nav_items->top_info_defined;
$et_slide_header = 'slide' === et_get_option( 'header_style', 'left' ) || 'fullscreen' === et_get_option(
    'header_style', 'left' ) ? true : false;

ob_start();
?>
<div class="et_slide_in_menu_container">
    <?php if ( 'fullscreen' === et_get_option( 'header_style', 'left' ) || is_customize_preview() ) { ?>
        <span class="mobile_menu_bar et_toggle_fullscreen_menu"></span>
    <?php } ?>

    <?php
    if ( $et_contact_info_defined || true === $show_header_social_icons || false !== et_get_option( 'show_search_icon', true ) || class_exists( 'woocommerce' ) || is_customize_preview() ) { ?>
    <div class="et_slide_menu_top">

        <?php if ( 'fullscreen' === et_get_option( 'header_style', 'left' ) ) { ?>
        <div class="et_pb_top_menu_inner">
            <?php } ?>
            <?php }

            if ( true === $show_header_social_icons ) {
                get_template_part( 'includes/social_icons', 'header' );
            }

            et_show_cart_total();
            ?>
            <?php if ( false !== et_get_option( 'show_search_icon', true ) || is_customize_preview() ) : ?>
                <?php if ( 'fullscreen' !== et_get_option( 'header_style', 'left' ) ) { ?>
                    <div class="clear"></div>
                <?php } ?>
                <form role="search" method="get" class="et-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <?php
                    printf( '<input type="search" class="et-search-field" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
                        esc_attr__( 'Search &hellip;', 'Divi' ),
                        get_search_query(),
                        esc_attr__( 'Search for:', 'Divi' )
                    );

                    /**
                     * Fires inside the search form element, just before its closing tag.
                     *
                     * @since ??
                     */
                    do_action( 'et_search_form_fields' );
                    ?>
                    <button type="submit" id="searchsubmit_header"></button>
                </form>
            <?php endif; // true === et_get_option( 'show_search_icon', false ) ?>

            <?php if ( $et_contact_info_defined ) : ?>

                <div id="et-info">
                    <?php if ( '' !== ( $et_phone_number = et_get_option( 'phone_number' ) ) ) : ?>
                        <span id="et-info-phone"><?php echo et_core_esc_previously( et_sanitize_html_input_text( $et_phone_number ) ); ?></span>
                    <?php endif; ?>

                    <?php if ( '' !== ( $et_email = et_get_option( 'header_email' ) ) ) : ?>
                        <a href="<?php echo esc_attr( 'mailto:' . $et_email ); ?>"><span id="et-info-email"><?php echo esc_html( $et_email ); ?></span></a>
                    <?php endif; ?>
                </div> <!-- #et-info -->

            <?php endif; // true === $et_contact_info_defined ?>
            <?php if ( $et_contact_info_defined || true === $show_header_social_icons || false !== et_get_option( 'show_search_icon', true ) || class_exists( 'woocommerce' ) || is_customize_preview() ) { ?>
            <?php if ( 'fullscreen' === et_get_option( 'header_style', 'left' ) ) { ?>
        </div> <!-- .et_pb_top_menu_inner -->
    <?php } ?>

    </div> <!-- .et_slide_menu_top -->
<?php } ?>

    <div class="et_pb_fullscreen_nav_container">
        <?php
        $slide_nav = '';
        $slide_menu_class = 'et_mobile_menu';

        $slide_nav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'echo' => false, 'items_wrap' => '%3$s' ) );
        $slide_nav .= wp_nav_menu( array( 'theme_location' => 'secondary-menu', 'container' => '', 'fallback_cb' => '', 'echo' => false, 'items_wrap' => '%3$s' ) );
        ?>

        <ul id="mobile_menu_slide" class="<?php echo esc_attr( $slide_menu_class ); ?>">

            <?php
            if ( '' === $slide_nav ) :
                ?>
                <?php if ( 'on' === et_get_option( 'divi_home_link' ) ) { ?>
                <li <?php if ( is_home() ) echo( 'class="current_page_item"' ); ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Home', 'Divi' ); ?></a></li>
            <?php }; ?>

                <?php show_page_menu( $slide_menu_class, false, false ); ?>
                <?php show_categories_menu( $slide_menu_class, false ); ?>
            <?php
            else :
                echo et_core_esc_wp( $slide_nav ) ;
            endif;
            ?>

        </ul>
    </div>
</div>
<?php
$slide_header = ob_get_clean();
/**
 * Filters the HTML output for the slide header.
 *
 * @since 3.10
 *
 * @param string $top_header
 */
echo et_core_intentionally_unescaped( apply_filters( 'et_html_slide_header', $slide_header ), 'html' );
?>